package main

import (
	"fmt"
	"math"
)

const pi = math.Pi


func f_solver(x float64,eps float64)(float64){ 

	f := math.Sin(pi*x / 180) - 1 / x;
	iter := 0
	for i := 1; (math.Abs(f) > eps) && (i < 20000); i++ {
		f = math.Sin(pi*x / 180) - 1 / x;
		df := pi / 180 * math.Cos(pi*x / 180) + 1 / (x*x);
		x = x - f / df;
		//fmt.Printf("i = %d\n", i)
		iter ++
	}
  	
  	fmt.Printf("iterations = %d\n", iter)
	return x;
}


func main() { 
	
	fmt.Printf("x0 = %.6f\n", f_solver(1, 0.000001))
	
}
