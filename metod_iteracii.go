package main

import (
	"fmt"
	"math"
)

const pi = math.Pi


func f_solver(x float64,eps float64)(float64){ 

	var rez float64
    iter := 0

	for i := 1; (math.Abs(rez - x) > eps) && (i < 20000); i++ {
		rez = x
    	x = 1 / (math.Sin(pi*x / 180)) // решаемое уравнение
		iter ++
	}
  	
  	fmt.Printf("iterations = %d\n", iter)
	return x;
}


func main() { 
	
	fmt.Printf("x0 = %.6f\n", f_solver(7, 0.00001))
	
}
