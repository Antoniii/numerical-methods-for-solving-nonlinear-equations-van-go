![](https://gitlab.com/Antoniii/numerical-methods-for-solving-nonlinear-equations-van-go/-/raw/main/нелинурав.PNG)

![](https://gitlab.com/Antoniii/numerical-methods-for-solving-nonlinear-equations-van-go/-/raw/main/метод-Ньютона.PNG)

![](https://gitlab.com/Antoniii/numerical-methods-for-solving-nonlinear-equations-van-go/-/raw/main/hords.PNG)

![](https://gitlab.com/Antoniii/numerical-methods-for-solving-nonlinear-equations-van-go/-/raw/main/dihoto.PNG)

## Sources

* [Численные методы решения нелинейных уравнений](https://prog-cpp.ru/digital-find/)
* [Go Math](https://pkg.go.dev/math#Abs)
* [Pi value in Go (Golang)](https://golangbyexample.com/pi-value-golang/)
